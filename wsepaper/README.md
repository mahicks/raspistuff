# WaveShare E-Paper Displays for RaspberryPi

This small C++ library allows for easy rendering of simple
framebuffers to the WaveShare E-Paper displays, namely the:

* 2.13inch E-Ink display - 212x104 pixels 
* 2.7inch E-Ink display - 264x176 pixels

This library uses, and depends on, the wiringPi library.

Example included.

Datasheets and information can be found at the vendor sites:

https://www.waveshare.com/wiki/2.13inch_e-Paper_HAT_(B)

https://www.waveshare.com/wiki/2.7inch_e-Paper_HAT_(B)

