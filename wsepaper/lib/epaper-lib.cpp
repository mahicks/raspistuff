#include "epaper-lib.h"
#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <array>
#include <limits>
#include <math.h>
using namespace wsepaper;

// Information taken from: https://www.waveshare.com/wiki

// pins
static constexpr unsigned char RST_PIN  {17};
static constexpr unsigned char DC_PIN   {25};
static constexpr unsigned char BUSY_PIN {24};

// waveshare epaper commands
static constexpr unsigned char PANEL_SETTING                  {0x00};
static constexpr unsigned char POWER_OFF                      {0x02};
static constexpr unsigned char POWER_ON                       {0x04};
static constexpr unsigned char BOOSTER_SOFT_START             {0x06};
static constexpr unsigned char DEEP_SLEEP                     {0x07};
static constexpr unsigned char DATA_START_TRANSMISSION_1      {0x10};
static constexpr unsigned char DISPLAY_REFRESH                {0x12};
static constexpr unsigned char DATA_START_TRANSMISSION_2      {0x13};
static constexpr unsigned char VCOM_AND_DATA_INTERVAL_SETTING {0x50};
static constexpr unsigned char RESOLUTION_SETTING             {0x61};

// waveshape epaper27 commands
static constexpr unsigned char TCON_RESOLUTION                {0x61};
static constexpr unsigned char PLL_CONTROL                    {0x30};
static constexpr unsigned char POWER_SETTING                  {0x01};
static constexpr unsigned char VCM_DC_SETTING_REGISTER        {0x82};
static constexpr unsigned char PARTIAL_DISPLAY_REFRESH        {0x16};
static constexpr unsigned char LUT_FOR_VCOM                   {0x20};
static constexpr unsigned char LUT_WHITE_TO_WHITE             {0x21};
static constexpr unsigned char LUT_BLACK_TO_WHITE             {0x22};
static constexpr unsigned char LUT_WHITE_TO_BLACK             {0x23};
static constexpr unsigned char LUT_BLACK_TO_BLACK             {0x24};

// epaper27 bonus buttons
static constexpr std::array<unsigned char,4> KEYS             {5,6,13,19};

//-------------------LUTs--------------------------
constexpr unsigned char lut_vcom_dc[] =
  { 0x00, 0x00, 0x00, 0x1A, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x0A, 0x0A, 0x00,
    0x00, 0x08, 0x00, 0x0E, 0x01, 0x0E, 0x01, 0x10, 0x00, 0x0A, 0x0A, 0x00,
    0x00, 0x08, 0x00, 0x04, 0x10, 0x00, 0x00, 0x05, 0x00, 0x03, 0x0E, 0x00,
    0x00, 0x0A, 0x00, 0x23, 0x00, 0x00,0x00, 0x01 };

constexpr unsigned char lut_ww[] =
  { 0x90, 0x1A, 0x1A, 0x00, 0x00, 0x01, 0x40, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x84, 0x0E, 0x01, 0x0E, 0x01, 0x10, 0x80, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x00, 0x04, 0x10, 0x00, 0x00, 0x05, 0x00, 0x03, 0x0E, 0x00, 0x00, 0x0A,
    0x00, 0x23, 0x00, 0x00, 0x00, 0x01 };

constexpr unsigned char lut_bw[] =
  { 0xA0, 0x1A, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x84, 0x0E, 0x01, 0x0E, 0x01, 0x10, 0x90, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0xB0, 0x04, 0x10, 0x00, 0x00, 0x05, 0xB0, 0x03, 0x0E, 0x00, 0x00, 0x0A,
    0xC0, 0x23, 0x00, 0x00, 0x00, 0x01 };

constexpr unsigned char lut_bb[] =
  { 0x90, 0x1A, 0x1A, 0x00, 0x00, 0x01, 0x40, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x84, 0x0E, 0x01, 0x0E, 0x01, 0x10, 0x80, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x00, 0x04, 0x10, 0x00, 0x00, 0x05, 0x00, 0x03, 0x0E, 0x00, 0x00, 0x0A,
    0x00, 0x23, 0x00, 0x00, 0x00, 0x01 };

constexpr unsigned char lut_wb[] =
  { 0x90, 0x1A, 0x1A, 0x00, 0x00, 0x01, 0x20, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x84, 0x0E, 0x01, 0x0E, 0x01, 0x10, 0x10, 0x0A, 0x0A, 0x00, 0x00, 0x08,
    0x00, 0x04, 0x10, 0x00, 0x00, 0x05, 0x00, 0x03, 0x0E, 0x00, 0x00, 0x0A,
    0x00, 0x23, 0x00, 0x00, 0x00, 0x01 };
//-------------------------------------------------

static void writecommand (unsigned char command)
{
  digitalWrite (DC_PIN, LOW);
  wiringPiSPIDataRW (0, &command, 1);
}

static void writedata (unsigned char data)
{
  digitalWrite (DC_PIN, HIGH);
  wiringPiSPIDataRW (0, &data, 1);
}

static void idlewait ()
{
  while (digitalRead (BUSY_PIN) == 0)
  {
    delay (100);
  }
}

static void render (const epaper::fbdata_t * onebitfb, bool isred,
                    epaper::extent_t width, epaper::extent_t height)
{
  if (isred)
  {
    writecommand (DATA_START_TRANSMISSION_2);
  }
  else
  {
    writecommand (DATA_START_TRANSMISSION_1);
  }
  delay (2);
  for (unsigned i = 0; i < width * height / 8; i++)
  {
    writedata (onebitfb[i]);
  }
  delay (2);
  writecommand (DISPLAY_REFRESH);
  idlewait ();
}

epaper::fbdata_t * epaper::newFB (epaper::extent_t width,
                                  epaper::extent_t height)
{
  return new fbdata_t[(width * height) / 8 + 1];
}

void epaper::initFB (epaper::fbdata_t * onebitfb, epaper::extent_t width,
                     epaper::extent_t height, epaper::pixel_t lowhigh)
{
  const epaper::extent_t extent {(width * height) / 8};
  const epaper::fbdata_t initv = lowhigh ? 0xFF : 0x00;
  for (epaper::extent_t i = 0; i < extent; i++)
  {
    onebitfb[i] = initv;
  }
}

void epaper::invertFB (epaper::fbdata_t * onebitfb, epaper::extent_t width,
                       epaper::extent_t height)
{
  const epaper::extent_t extent {(width * height) / 8};
  for (epaper::extent_t i = 0; i < extent; i++)
  {
    onebitfb[i] = ~(onebitfb[i]);
  }
}

static constexpr epaper::extent_t calcchar (epaper::extent_t width,
                    epaper::extent_t xpos, epaper::extent_t ypos)
{
  return (xpos + (ypos * width)) / 8;
}

// Pre-Condition: mask has only 1 bit out of its 8 bits set to 1
static constexpr epaper::fbdata_t quickrevmask (epaper::fbdata_t mask)
{
  return (1 << (std::numeric_limits<decltype(mask)>::digits - 1)) / mask;
}

epaper::fbdata_t calcbitmask (epaper::extent_t xpos)
{
  return quickrevmask (pow (2, xpos % 8));
}

void epaper::setPixel (epaper::fbdata_t * onebitfb, epaper::extent_t width,
                       epaper::extent_t xpos, epaper::extent_t ypos,
                       epaper::pixel_t lowhigh)
{
  auto charpos = calcchar (width, xpos, ypos);
  auto bitmask = calcbitmask (xpos);
  if (lowhigh)
  {
    onebitfb[charpos] |= bitmask;
  }

  else
  {
    onebitfb[charpos] &= (~bitmask);
  }
}

epaper::pixel_t epaper::getPixel (const epaper::fbdata_t * onebitfb,
                                  epaper::extent_t width,
                                  epaper::extent_t xpos,
                                  epaper::extent_t ypos)
{
  auto charpos = calcchar (width, xpos, ypos);
  auto bitmask = calcbitmask (xpos);
  return (onebitfb[charpos] & bitmask) > 0;
}

void  epaper::render_black (const epaper::fbdata_t * onebitfbblack,
                            epaper::extent_t width, epaper::extent_t height)
{
  render (onebitfbblack, false, width, height);
}

void epaper::render_red (const epaper::fbdata_t * onebitfbred,
                         epaper::extent_t width, epaper::extent_t height)
{
  render (onebitfbred, true, width, height);
}

void epaper213::initialize (epaper::extent_t width, epaper::extent_t height)
{
  // configure wiringpi
  wiringPiSetupGpio ();
  pinMode (RST_PIN, OUTPUT);
  pinMode (DC_PIN, OUTPUT);
  pinMode (BUSY_PIN, INPUT);
  wiringPiSPISetup (0, 2000000);

  // perform reset
  digitalWrite (RST_PIN, LOW);
  delay (200);
  digitalWrite (RST_PIN, HIGH);
  delay (200);

  // configure waveshare epaper
  writecommand (BOOSTER_SOFT_START);
  writedata (0x17);
  writedata (0x17);
  writedata (0x17);
  writecommand (POWER_ON);
  idlewait ();
  writecommand (PANEL_SETTING);
  writedata (0x8F);
  writecommand (VCOM_AND_DATA_INTERVAL_SETTING);
  writedata (0x37);
  writecommand (RESOLUTION_SETTING);
  writedata (width);
  writedata (0x00);
  writedata (height);
}

void epaper213::deinitialize ()
{
  writecommand (POWER_OFF);
  idlewait ();
  writecommand (DEEP_SLEEP);
  writedata (0xA5);
}

void writelut ()
{
  writecommand (LUT_FOR_VCOM);
  for (int count = 0; count < 44; count++)
  {
    writedata (lut_vcom_dc[count]);
  }
  writecommand (LUT_WHITE_TO_WHITE);
  for (int count = 0; count < 42; count++)
  {
    writedata (lut_ww[count]);
  }
  writecommand (LUT_BLACK_TO_WHITE);
  for (int count = 0; count < 42; count++)
  {
    writedata (lut_bw[count]);
  }
  writecommand (LUT_WHITE_TO_BLACK);
  for (int count = 0; count < 42; count++)
  {
    writedata (lut_bb[count]);
  }
  writecommand (LUT_BLACK_TO_BLACK);
  for (int count = 0; count < 42; count++)
  {
    writedata (lut_wb[count]);
  }
}

static void dokeyinit()
{
  for (auto keypin : KEYS)
  {
    pinMode (keypin, INPUT);
    pullUpDnControl(keypin,PUD_UP);
  }
}

void epaper27::initialize (epaper::extent_t width, epaper::extent_t height)
{
  // configure wiringPi
  wiringPiSetupGpio ();
  pinMode (RST_PIN, OUTPUT);
  pinMode (DC_PIN, OUTPUT);
  pinMode (BUSY_PIN, INPUT);
  dokeyinit();
  // complete wiringPi setup
  wiringPiSPISetup (0, 2000000);

  // perform reset
  digitalWrite (RST_PIN, LOW);
  delay (200);
  digitalWrite (RST_PIN, HIGH);
  delay (200);

  // init diplay itself
  writecommand (POWER_ON);
  idlewait ();
  writecommand (PANEL_SETTING);
  writedata (0xaf);
  writecommand (PLL_CONTROL);
  writedata (0x3a);
  writecommand (POWER_SETTING);
  writedata (0x03);
  writedata (0x00);
  writedata (0x2b);
  writedata (0x2b);
  writedata (0x09);
  writecommand (BOOSTER_SOFT_START);
  writedata (0x07);
  writedata (0x07);
  writedata (0x17);

  // 'Power optimizations'
  writecommand (0xF8);
  writedata (0x60);
  writedata (0xA5);
  writecommand (0xF8);
  writedata (0x89);
  writedata (0xA5);
  writecommand (0xF8);
  writedata (0x90);
  writedata (0x00);
  writecommand (0xF8);
  writedata (0x93);
  writedata (0x2A);
  writecommand (0xF8);
  writedata (0x73);
  writedata (0x41);
  writecommand (VCM_DC_SETTING_REGISTER);
  writedata (0x12);
  writecommand (VCOM_AND_DATA_INTERVAL_SETTING);
  writedata (0x87);
  writelut ();
  writecommand (PARTIAL_DISPLAY_REFRESH);
  writedata (0x00);

  // configures for full-frame
  writecommand (TCON_RESOLUTION);
  writedata (width >> 8);
  writedata (width & 0xff);
  writedata (height >> 8);
  writedata (height & 0xff);
}

void epaper27::deinitialize ()
{
  writecommand (DEEP_SLEEP);
  writedata (0xA5);
}

static void waitpin (unsigned pin)
{
  while (digitalRead (pin) == 1)
  {
    delay (100);
  }
}

void epaper27::initialize_keys_only ()
{
  // configure wiringPi
  wiringPiSetupGpio ();
  dokeyinit();
  // complete wiringPi setup
  wiringPiSPISetup (0, 2000000);
}

void epaper27::waitkey (unsigned char keynumber)
{
  waitpin(KEYS.at(keynumber-1));
}

