#ifndef WAVESHARE_PAPER_H
#define WAVESHARE_PAPER_H
namespace wsepaper
{
  class epaper
  {
  public:
    // types for use with wsepaper
    typedef unsigned char fbdata_t;
    typedef unsigned int  extent_t;
    typedef bool pixel_t;
    epaper () = delete;
    // convenience methods for interacting with fb
    static fbdata_t *newFB (extent_t width, extent_t height);
    static void initFB (fbdata_t * onebitfb, extent_t width, extent_t height,
                        pixel_t lowhigh);
    static void invertFB (fbdata_t * onebitfb, extent_t width,
                          extent_t height);
    static void setPixel (fbdata_t * onebitfb, extent_t width, extent_t xpos,
                          extent_t ypos, pixel_t loworhigh);
    static pixel_t getPixel (const fbdata_t * onebitfb, extent_t width,
                             extent_t xpos, extent_t ypos);
    // generic epaper render methods
    static void render_black (const fbdata_t * onebitfbblack, extent_t width,
                              extent_t height);
    static void render_red (const fbdata_t * onebitfbred, extent_t width,
                            extent_t height);
  };

  class epaper213: public epaper
  {
  public:
    epaper213 () = delete;
    static constexpr pixel_t  pixel_low  {false};
    static constexpr pixel_t  pixel_high {true};
    static constexpr unsigned dwidth    = 104;
    static constexpr unsigned dheight   = 212;
    static void initialize (extent_t width = dwidth, extent_t height =
                            dheight);
    static void deinitialize ();
  };

  class epaper27: public epaper
  {
  public:
    epaper27 () = delete;
    static constexpr pixel_t  pixel_low  {true};
    static constexpr pixel_t  pixel_high {false};
    static constexpr unsigned dwidth    = 176;
    static constexpr unsigned dheight   = 264;
    static void initialize (extent_t width = dwidth, extent_t height =
                            dheight);
    static void deinitialize ();
    static void initialize_keys_only();
    static void waitkey (unsigned char keynumber);
  };
}             // wsepaper namespace
#endif        // WAVESHARE_PAPER_H
