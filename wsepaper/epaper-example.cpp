#include <epaper-lib.h>

// define which type of epaper is connected
#define EPTYPE epaper27 // or epaper213

using namespace wsepaper;

int main (void)
{
  // create a new framebuffer
  epaper::fbdata_t * renderdata =
    epaper::newFB (EPTYPE::dwidth, EPTYPE::dheight);
  // initialize it
  epaper::initFB (renderdata, EPTYPE::dwidth, EPTYPE::dheight,
                  EPTYPE::pixel_high);

  // prepare epaper for first use
  EPTYPE::initialize ();

  // render bw framebuffer channel
  epaper::render_black (renderdata, EPTYPE::dwidth, EPTYPE::dheight);
 
  // create a regular matrix of red points
  for (epaper::extent_t c = 0; c < EPTYPE::dwidth; c += 2)
  {
    for (epaper::extent_t r = 0; r < EPTYPE::dheight; r += 2)
    {
      epaper::setPixel (renderdata, EPTYPE::dwidth, c, r,
                        EPTYPE::pixel_low);
    }
  }

  // render red framebuffer channel
  epaper::render_red (renderdata, EPTYPE::dwidth, EPTYPE::dheight);

  // put epaper to sleep
  EPTYPE::deinitialize ();

  return 0;
}
