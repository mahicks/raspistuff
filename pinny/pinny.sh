#!/bin/bash

set -eu

if [ $# -ne 3 ]; then
  echo "Usage: `basename $0` <read/write/clean> <pin> <low/high>"
  exit 1
fi

PIN_MODE="$1"
PIN_NUM="$2"
PIN_LEVEL="$3"

# use SYSFS to set the pin behaviour
if [ ! -d /sys/class/gpio/gpio$PIN_NUM ]; then
  set +e
  echo "$PIN_NUM" > /sys/class/gpio/export
  if [ $? -ne 0 ]; then
    echo "Invalid pin: $PIN_NUM" 1>&2
    exit 1
  fi
  set -e
fi

case "$PIN_MODE" in
  "read")
    echo "in" > /sys/class/gpio/gpio$PIN_NUM/direction
    ;;
  "write")
    echo "out" > /sys/class/gpio/gpio$PIN_NUM/direction
    ;;
  "clean")
    # if this is a clean operation then just clean and exit
    echo "$PIN_NUM" > /sys/class/gpio/unexport
    exit
    ;;
  *)
    echo "Invalid pin mode: $PIN_MODE" 1>&2
    exit 5
    ;;
esac


# write out (or pull up) to SYSFS
case "$PIN_LEVEL" in
  "low")
    echo "0" > /sys/class/gpio/gpio$PIN_NUM/value
    ;;
  "high")
    echo "1" > /sys/class/gpio/gpio$PIN_NUM/value
    ;;
  *)
    echo "Invalid pin level: $PIN_LEVEL" 1>&2
    exit 6
    ;;
esac

# perform read if necessary
if [ "$PIN_MODE" = "read" ]; then
  cat /sys/class/gpio/gpio$PIN_NUM/value
fi

# cleanup in SYSFS
#echo "$PIN_NUM" > /sys/class/gpio/unexport
